data "aws_caller_identity" "current" {}

data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = module.vpc.vpc_id
}

resource "random_id" "stack_id" {
  byte_length = 8
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "${data.aws_caller_identity.current.account_id}-${var.aws_region}-tf-state"
  # state files
  versioning {
    enabled = true
  } # Enable server-side encryption by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name         = "${data.aws_caller_identity.current.account_id}-${var.aws_region}-tf-lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.48"

  name = "vpc-${var.environment}"
  cidr = "10.0.0.0/16"

  azs             = [data.aws_availability_zones.available.names[0]]
  private_subnets = ["10.0.1.0/24"]
  public_subnets  = ["10.0.101.0/24"]

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_s3_endpoint = true

  tags = {
    Environment = var.environment
  }
}

resource "aws_iam_instance_profile" "runners_profile" {
  name = "gitlab_runners_profile-${random_id.stack_id.hex}"
  role = aws_iam_role.runners_role.name
}

resource "aws_iam_role" "runners_role" {
  name = "gitlab_runners_role-${random_id.stack_id.hex}"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "runners_role_admin_attach" {
  role       = aws_iam_role.runners_role.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

module "runner" {
  source = "npalm/gitlab-runner/aws"

  aws_region  = var.aws_region
  environment = "${var.environment}-${random_id.stack_id.hex}"

  vpc_id                   = module.vpc.vpc_id
  subnet_ids_gitlab_runner = module.vpc.private_subnets
  subnet_id_runners        = element(module.vpc.private_subnets, 0)
  metrics_autoscaling      = ["GroupDesiredCapacity", "GroupInServiceCapacity"]

  runners_name             = "${var.runner_name}-${random_id.stack_id.hex}"
  runners_gitlab_url       = var.gitlab_url
  enable_runner_ssm_access = true

  cache_bucket_prefix = "${var.runner_name}-${random_id.stack_id.hex}"

  gitlab_runner_security_group_ids = [data.aws_security_group.default.id]

  docker_machine_download_url   = "https://gitlab-docker-machine-downloads.s3.amazonaws.com/v0.16.2-gitlab.2/docker-machine"
  docker_machine_spot_price_bid = "0.06"

  gitlab_runner_registration_config = {
    registration_token = var.registration_token
    tag_list           = "dev"
    description        = "runner default - auto"
    locked_to_project  = "true"
    run_untagged       = "true"
    maximum_timeout    = "3600"
  }

  tags = {
    "tf-aws-gitlab-runner:example"           = "runner-default"
    "tf-aws-gitlab-runner:instancelifecycle" = "spot:yes"
  }

  runners_privileged                = "true"
  runners_additional_volumes        = ["/certs/client"]
  runners_iam_instance_profile_name = aws_iam_instance_profile.runners_profile.name

  runners_volumes_tmpfs = [
    {
      volume  = "/var/opt/cache",
      options = "rw,noexec"
    }
  ]

  runners_services_volumes_tmpfs = [
    {
      volume  = "/var/lib/mysql",
      options = "rw,noexec"
    }
  ]

  runners_machine_autoscaling = [
    {
      periods    = ["\"* * 0-9,17-23 * * mon-fri *\"", "\"* * * * * sat,sun *\""]
      idle_count = 0
      idle_time  = 60
      timezone   = var.timezone
    }
  ]
}

resource "null_resource" "cancel_spot_requests" {
  # Cancel active and open spot requests, terminate instances
  triggers = {
    environment = var.environment
  }

  provisioner "local-exec" {
    when    = destroy
    command = "./cancel-spot-instances.sh ${self.triggers.environment}"
  }
}
