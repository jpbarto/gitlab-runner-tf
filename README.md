# Terraform to create a GitLab Runner on AWS

This code uses the npalm/terraform-aws-gitlab-runner TF module to deploy a GitLab runner to AWS.  The runner uses Docker containers and Spot instances to execute build jobs.

For more information see: https://github.com/npalm/terraform-aws-gitlab-runner/tree/develop/examples/runner-default
