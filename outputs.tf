output "backend_s3_bucket" {
    value = aws_s3_bucket.terraform_state.id
}

output "backend_dynamodb_table" {
    value = aws_dynamodb_table.terraform_locks.name
}

output "backend_aws_region" {
    value = var.aws_region
}
